#include "fireball.h"
#include "constants.h"

Fireball::Fireball(SDL_Renderer* renderer)
{
    this->renderer = renderer;

    SDL_Surface* surface = IMG_Load("resources/fireball-no-bg.png");
    if (surface == NULL)
    {
        printf("Failed to load fireball image! Error: %s\n", IMG_GetError());
    }
    else
    {
        texture = SDL_CreateTextureFromSurface(renderer, surface);
        if (texture == NULL)
        {
            printf("Failed to create fireball texture! Error: %s\n", SDL_GetError());
        }
        else
        {
            SDL_FreeSurface(surface);
        }
    }

    velx = SCREEN_WIDTH / STEPS;
    vely = 0;
}

void Fireball::showTexture()
{
    rect = {this->posx, this->posy, 30, 30};
    collider = {this->posx + 15, this->posy + 15};

    SDL_RenderCopyEx(renderer, texture, NULL, &rect, angle, NULL, SDL_FLIP_NONE);
}

void Fireball::calculatePositionAndDirection(int posx, int posy, int angle)
{
    this->posx = posx;
    this->posy = posy;

    if (angle == 300)
    {
        vely = -1 * SCREEN_HEIGHT / STEPS;
    }
    else if (angle == 60)
    {
        vely = SCREEN_HEIGHT / STEPS;
    }
    else
    {
        vely = 0;
    }
}

void Fireball::draw(int posx, int posy, int angle)
{
    if (shootActive)
    {
        showTexture();
    }

    ++frameNo;

    if (frameNo % 45 != 0)
    {
        return;
    }

    if (!shootActive)
    {
        calculatePositionAndDirection(posx, posy, angle);
    }
    else
    {
        this->posx += velx;
        this->posy += vely;

        if (this->posx > SCREEN_WIDTH || this->posy < 0 || this->posy > SCREEN_HEIGHT)
        {
            shootActive = false;
            return;
        }
    }
}

bool Fireball::checkCollision(Bird* bird)
{
    if (shootActive && collider.x + 15 >= bird->getCollider().x + 40 && (collider.y - 15 >= bird->getCollider().y && collider.y + 15 < bird->getCollider().y + bird->getCollider().h))
    {
        shootActive = false;

        return true;
    }

    return false;
}

void Fireball::fire()
{
    shootActive = true;
}

void Fireball::free()
{
    SDL_DestroyTexture(texture);
    texture = NULL;
    renderer = NULL;
}
