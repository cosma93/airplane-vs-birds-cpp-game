#include "gameaudio.h"

GameAudio* GameAudio::instance = NULL;

GameAudio::GameAudio()
{
    propeller = Mix_LoadMUS("resources/propeller.mp3");
    if (propeller == NULL)
    {
        printf("Failed to load propeller sound! Error: %s", Mix_GetError());
    }
}

GameAudio* GameAudio::getInstance()
{
    if (instance == NULL)
    {
        instance = new GameAudio();
    }

    return instance;
}

void GameAudio::play()
{
    Mix_PlayMusic(propeller, -1);
}

void GameAudio::free()
{
    Mix_FreeMusic(propeller);
    propeller = NULL;
}
