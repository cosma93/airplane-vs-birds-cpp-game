#ifndef BIRDS_H_INCLUDED
#define BIRDS_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <vector>

const int NO_OF_FRAMES = 10;

class Bird
{
private:
    SDL_Texture* textures[NO_OF_FRAMES];
    SDL_Renderer* renderer;
    SDL_Window* window;
    SDL_Rect rect;
    SDL_Point spawnPoint;
    SDL_Point destination;

    int posx, posy;
    int angle = 0;
    bool spawned = false;

    int frameToRender = 0;
    unsigned long frames = 0;

    SDL_Rect collider;

    std::vector<SDL_Point> spawnPlaces;
    std::vector<SDL_Point> destinationPlaces;

    void spawn();
    void createSpawnAndDestinationPlaces();

public:
    Bird(SDL_Renderer* renderer, SDL_Window* window);
    void draw(int angle);
    void move();
    void free();
    void destroy();

    SDL_Rect getCollider();
};

#endif // BIRDS_H_INCLUDED
