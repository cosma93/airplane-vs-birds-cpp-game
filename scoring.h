#ifndef SCORING_H_INCLUDED
#define SCORING_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

class Scoring
{
private:
    static Scoring* instance;
    SDL_Texture* digitsTextures[10];
    SDL_Texture* minusTexture;
    SDL_Renderer* renderer;

    int score;

public:
    Scoring();
    static Scoring* getInstance();
    void init(SDL_Renderer* renderer);

    void increase();
    void decrease();

    void displayScore();

    void free();
};

#endif // SCORING_H_INCLUDED
