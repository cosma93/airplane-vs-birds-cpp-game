#include "bird.h"
#include "constants.h"
#include "scoring.h"
#include <string>
#include <sstream>

Bird::Bird(SDL_Renderer* renderer, SDL_Window* window)
{
    this->renderer = renderer;
    this->window = window;

    posx = SCREEN_WIDTH - 240;
    posy = SCREEN_HEIGHT - 180;

    rect = {posx, posy, 240, 180};
    collider = {posx, posy, 180, 135};

    SDL_Surface* surface = IMG_Load("resources/Bird.gif");
    for (int i = 0; i < NO_OF_FRAMES; ++i)
    {
        std::stringstream file;
        file << "resources/flying-bird/frame_0" << i << "_delay-0.15s.png";
        SDL_Surface* surface = IMG_Load(file.str().c_str());
        if (surface == NULL)
        {
            printf("Failed to load Bird frame %d! Error: %s", i, IMG_GetError());
        }
        else
        {
            Uint32 colorkey = SDL_MapRGB(surface->format, 0xFF, 0xFF, 0xFF);
            SDL_SetColorKey(surface, SDL_TRUE, colorkey);

            textures[i] = SDL_CreateTextureFromSurface(renderer, surface);
            if (textures[i] == NULL)
            {
                printf("Failed to create texture for frame no. %d! Error: %s", i, SDL_GetError());
            }
            else
            {
                SDL_FreeSurface(surface);
            }
        }
    }

    createSpawnAndDestinationPlaces();
}

void Bird::createSpawnAndDestinationPlaces()
{
    SDL_Point sp2 = {SCREEN_WIDTH, 0};
    SDL_Point sp3 = {SCREEN_WIDTH, SCREEN_HEIGHT / 2};
    SDL_Point sp4 = {SCREEN_WIDTH, SCREEN_HEIGHT};

    SDL_Point dp1 = {-rect.w, -rect.h};
    SDL_Point dp2 = {-rect.w, SCREEN_HEIGHT / 2};
    SDL_Point dp3 = {-rect.w, SCREEN_HEIGHT};
    SDL_Point dp4 = {SCREEN_WIDTH / 2 - rect.w, 0};
    SDL_Point dp5 = {SCREEN_WIDTH / 2 - rect.w, SCREEN_HEIGHT};

    spawnPlaces.push_back(sp2);
    spawnPlaces.push_back(sp3);
    spawnPlaces.push_back(sp4);

    destinationPlaces.push_back(dp1);
    destinationPlaces.push_back(dp2);
    destinationPlaces.push_back(dp3);
    destinationPlaces.push_back(dp4);
    destinationPlaces.push_back(dp5);
}

void Bird::spawn()
{
    if (!spawned)
    {
        SDL_Point sp = spawnPlaces[rand() % spawnPlaces.size()];
        SDL_Point dp = destinationPlaces[rand() % destinationPlaces.size()];

        bool spawnAgain = (sp.y == dp.y && sp.y != SCREEN_HEIGHT / 2) || (sp.x == dp.x + rect.w);
        while (spawnAgain)
        {
            sp = spawnPlaces[rand() % spawnPlaces.size()];
            dp = destinationPlaces[rand() % destinationPlaces.size()];

            spawnAgain = (sp.y == dp.y && sp.y != SCREEN_HEIGHT / 2) || (sp.x == dp.x + rect.w);
        }

        rect.x = sp.x;
        rect.y = sp.y;

        collider.x = sp.x;
        collider.y = sp.y;

        spawnPoint = sp;
        destination = dp;

        spawned = true;
    }
}

void Bird::move()
{
    const int STEPS = 128;

    int velx = SCREEN_WIDTH / STEPS;
    int vely = SCREEN_HEIGHT / STEPS;

    if (spawnPoint.y == destination.y)
    {
        vely = 0;
    }
    else if (spawnPoint.y > destination.y)
    {
        vely *= -1;
    }

    rect.x = rect.x - velx;
    rect.y = rect.y + vely;

    collider.x = collider.x - velx;
    collider.y = collider.y + vely;

    if (rect.x < destination.x)
    {
        spawned = false;
        Scoring::getInstance()->decrease();
    }
}

void Bird::draw(int angle)
{
    spawn();

    ++frames;
    if (frames % 128 == 0)
    {
        frameToRender++;
        frames = 0;
    }
    if (frameToRender == NO_OF_FRAMES) frameToRender = 0;

    SDL_RenderCopyEx(renderer, textures[frameToRender], NULL, &rect, angle, NULL, SDL_FLIP_NONE);
}

SDL_Rect Bird::getCollider()
{
    return collider;
}

void Bird::destroy()
{
    spawned = false;
}

void Bird::free()
{
    for (int i = 0; i < NO_OF_FRAMES; ++i)
    {
        SDL_DestroyTexture(textures[i]);
    }
}
