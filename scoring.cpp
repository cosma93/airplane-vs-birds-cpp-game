#include "scoring.h"
#include <string>
#include <sstream>
#include "constants.h"
#include <stdlib.h>
#include <vector>

Scoring* Scoring::instance = NULL;

Scoring::Scoring()
{
    score = 0;
}

Scoring* Scoring::getInstance()
{
    if (instance == NULL)
    {
        instance = new Scoring();
    }

    return instance;
}

void Scoring::init(SDL_Renderer* renderer)
{
    this->renderer = renderer;

    for (int i = 0; i < 10; ++i)
    {
        std::stringstream file;
        file << "resources/numbers/" << i << ".png";
        SDL_Surface* surface = IMG_Load(file.str().c_str());
        if (surface == NULL)
        {
            printf("Failed to load number %d image! Error: %s\n", i, IMG_GetError());
        }
        else
        {
            digitsTextures[i] = SDL_CreateTextureFromSurface(renderer, surface);
            if (digitsTextures[i] == NULL)
            {
                printf("Failed to create number %d texture! Error: %s\n", i, IMG_GetError());
            }
            else
            {
                SDL_FreeSurface(surface);
            }
        }
    }

    SDL_Surface* surface = IMG_Load("resources/numbers/minus.png");
    if (surface == NULL)
    {
        printf("Failed to load minus image! Error: %s\n", IMG_GetError());
    }
    else
    {
        minusTexture = SDL_CreateTextureFromSurface(renderer, surface);
        if (minusTexture == NULL)
        {
            printf("Failed to create minus texture! Error: %s\n", IMG_GetError());
        }
        else
        {
            SDL_FreeSurface(surface);
        }
    }
}

void Scoring::displayScore()
{
    int lastDigit = score % 10;
    int rest = score;

    std::vector<int> digits;

    while (rest != 0)
    {
        lastDigit = rest % 10;
        rest = rest / 10;

        digits.push_back(abs(lastDigit));
    }

    if (score == 0)
    {
        digits.push_back(0);
    }

    int x = 20;
    if (score < 0)
    {
        SDL_Rect rect = {x, SCREEN_HEIGHT - 80, 60, 80};
        SDL_RenderCopy(renderer, minusTexture, NULL, &rect);

        x += 60;
    }

    while (!digits.empty())
    {
        int digit = digits.back();
        digits.pop_back();

        SDL_Rect rect = {x, SCREEN_HEIGHT - 80, 60, 80};
        SDL_RenderCopy(renderer, digitsTextures[digit], NULL, &rect);

        x += 60;
    }
}

void Scoring::increase()
{
    score += 50;
}

void Scoring::decrease()
{
    score -= 100;
}

void Scoring::free()
{
    for (int i = 0; i < 10; ++i)
    {
        SDL_DestroyTexture(digitsTextures[i]);
        digitsTextures[i] = NULL;
    }

    renderer = NULL;
}
