#ifndef AIRPLANE_H_INCLUDED
#define AIRPLANE_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "fireball.h"

class Airplane
{
public:
    Airplane(SDL_Renderer* renderer);
    void draw();
    void handleKeys(const Uint8* keyStates);
    void free();

    void shoot();
    void targetHit(Bird* bird);

private:
    SDL_Texture* texture;
    SDL_Renderer* renderer;
    SDL_Rect rect;

    Fireball* fb;

    int posx, posy;
    const int SPEED = 1;
    int angle = 0;
};

#endif // AIRPLANE_H_INCLUDED
