#ifndef FIREBALL_H_INCLUDED
#define FIREBALL_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "bird.h"

class Fireball
{
public:
    Fireball();
    Fireball(SDL_Renderer* renderer);
    void draw(int posx, int posy, int angle);
    void free();

    bool checkCollision(Bird* bird);
    void fire();

private:
    void showTexture();
    void calculatePositionAndDirection(int posx, int posy, int angle);

    SDL_Texture* texture;
    SDL_Renderer* renderer;
    SDL_Rect rect;

    SDL_Point collider;

    int posx, posy;
    int velx, vely;
    int angle = 0;
    bool shootActive = false;
    unsigned int frameNo = 0;

    const int STEPS = 128;
};

#endif // FIREBALL_H_INCLUDED
