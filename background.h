#ifndef BACKGROUND_H_INCLUDED
#define BACKGROUND_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

class Background
{
public:
    Background(SDL_Renderer* renderer);
    void show();
    void free();

private:
    SDL_Texture* texture;
    SDL_Renderer* renderer;

    int scrollOffset;
    const int SPEED = 11;
};

#endif // BACKGROUND_H_INCLUDED
