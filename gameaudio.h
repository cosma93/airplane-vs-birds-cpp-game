#ifndef GAMEAUDIO_H_INCLUDED
#define GAMEAUDIO_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

class GameAudio
{
private:
    static GameAudio* instance;
    Mix_Music* propeller;

public:
    GameAudio();
    static GameAudio* getInstance();
    void free();
    void play();
};

#endif // GAMEAUDIO_H_INCLUDED
