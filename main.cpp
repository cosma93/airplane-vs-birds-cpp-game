#include <exception>
#include <string>
#include <iostream>
#include "airplane.h"
#include "bird.h"
#include "background.h"
#include "constants.h"
#include "gameaudio.h"
#include "fireball.h"
#include "scoring.h"

SDL_Window* gWindow;
SDL_Renderer* gRenderer;

bool init()
{
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
    {
        printf("Failed to initialize! Error: %s\n", SDL_GetError());
        success = false;
    }

    gWindow = SDL_CreateWindow("My Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, 0);

    if (gWindow == NULL)
    {
        printf("Failed to create window! Error: %s\n", SDL_GetError());
        success = false;
    }
    else
    {
        gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
        if (gRenderer == NULL)
        {
            printf("Failed to create renderer! Error: %s\n", SDL_GetError());
            success = false;
        }
        else
        {
            SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

            int flags = IMG_INIT_JPG | IMG_INIT_PNG;
            int initted = IMG_Init(flags);
            if((initted & flags) != flags)
            {
                printf("IMG_Init: Failed to init required jpg and png support!\n");
                printf("IMG_Init: %s\n", IMG_GetError());
                success = false;
            }

            if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
            {
                printf("Failed to initialize audio! Error: %s\n", Mix_GetError());
                success = false;
            }

            Scoring::getInstance()->init(gRenderer);
        }
    }

    return success;
}

void close()
{
    SDL_DestroyRenderer(gRenderer);
    SDL_DestroyWindow(gWindow);
    gRenderer = NULL;
    gWindow = NULL;

    Mix_Quit();
    IMG_Quit();
    SDL_Quit();
}

int main(int argc, char* argv[])
{
    if (!init())
    {
        return 1;
    }
    else
    {
        bool quit = false;
        SDL_Event e;

        Airplane airplane(gRenderer);
        Bird bird(gRenderer, gWindow);
        Background background(gRenderer);

        GameAudio::getInstance()->play();

        int frameNo = 0;

        while (!quit)
        {
            while (SDL_PollEvent(&e) != 0)
            {
                if (e.type == SDL_QUIT)
                {
                    quit = true;
                }
            }

            ++frameNo;
            if (frameNo % 6 == 0)
            {
                const Uint8* keystates = SDL_GetKeyboardState(NULL);
                airplane.handleKeys(keystates);
            }
            if (frameNo % 64 == 0)
            {
                bird.move();
            }

            if (6 * 64 == frameNo)
            {
                frameNo = 0;
            }

            SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
            SDL_RenderClear(gRenderer);

            background.show();
            airplane.draw();

            bird.draw(0);
            airplane.targetHit(&bird);

            Scoring::getInstance()->displayScore();

            SDL_RenderPresent(gRenderer);
        }

        airplane.free();
        background.free();
        bird.free();
        Scoring::getInstance()->free();
    }

    close();

    return 0;
}
