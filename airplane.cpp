#include "airplane.h"
#include "constants.h"
#include "scoring.h"

Airplane::Airplane(SDL_Renderer* renderer)
{
    this->renderer = renderer;

    posx = 300;
    posy = 200;

    rect = {posx, posy, 140, 100};

    SDL_Surface* surface = IMG_Load("resources/airplane.png");
    if (surface == NULL)
    {
        printf("Failed to load airplane image! Error: %s\n", IMG_GetError());
    }
    else
    {
        texture = SDL_CreateTextureFromSurface(renderer, surface);
        if (texture == NULL)
        {
            printf("Failed to create airplane texture! Error: %s\n", SDL_GetError());
        }
        else
        {
            SDL_FreeSurface(surface);
        }
    }

    Fireball* fireball = new Fireball(renderer);
    this->fb = fireball;
}

void Airplane::draw()
{
    rect.x = posx;
    rect.y = posy;

    SDL_RenderCopyEx(renderer, texture, NULL, &rect, angle, NULL, SDL_FLIP_NONE);

    fb->draw(posx + rect.w, posy + rect.h / 2, angle);
}

void Airplane::handleKeys(const Uint8* keyStates)
{
    angle = 0;

    if (keyStates[SDL_SCANCODE_UP])
    {
        posy -= SPEED;
        angle = 300;
        if (posy < 0)
        {
            posy += SPEED;
            angle = 0;
        }
    }
    if (keyStates[SDL_SCANCODE_DOWN])
    {
        posy += SPEED;
        angle = 60;
        if (posy + rect.h > SCREEN_HEIGHT)
        {
            posy -= SPEED;
            angle = 0;
        }
    }
    if (keyStates[SDL_SCANCODE_LEFT])
    {
        posx -= SPEED;
        if (posx < 0)
        {
            posx += SPEED;
        }
    }
    if (keyStates[SDL_SCANCODE_RIGHT])
    {
        posx += SPEED;
        if (posx + rect.w > SCREEN_WIDTH)
        {
            posx -= SPEED;
        }
    }
    if (keyStates[SDL_SCANCODE_W])
    {
        fb->fire();
    }
}

void Airplane::targetHit(Bird* bird)
{
    if (fb->checkCollision(bird))
    {
        bird->destroy();
        Scoring::getInstance()->increase();
    }
}

void Airplane::free()
{
    SDL_DestroyTexture(texture);

    renderer = NULL;
    texture = NULL;
}
