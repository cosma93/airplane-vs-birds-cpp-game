#include "background.h"
#include "constants.h"

Background::Background(SDL_Renderer* renderer)
{
    this->renderer = renderer;
    scrollOffset = 0;

    SDL_Surface* surface = IMG_Load("resources/sky.jpg");
    if (surface == NULL)
    {
        printf("Failed to load background image! Error: %s\n", IMG_GetError());
    }
    else
    {
        texture = SDL_CreateTextureFromSurface(renderer, surface);
        if (texture == NULL)
        {
            printf("Failed to load background surface! Error: %s\n", SDL_GetError());
        }
        else
        {
            SDL_FreeSurface(surface);
        }
    }
}

void Background::show()
{
    SDL_Point textureSize;
    SDL_QueryTexture(texture, NULL, NULL, &textureSize.x, &textureSize.y);

    --scrollOffset;
    if (scrollOffset / SPEED < -SCREEN_WIDTH)
    {
        scrollOffset = 0;
    }

    SDL_Rect firstImage = {scrollOffset / SPEED, 0, SCREEN_WIDTH, SCREEN_HEIGHT};
    SDL_Rect secondImage = {scrollOffset / SPEED + SCREEN_WIDTH, 0, SCREEN_WIDTH, SCREEN_HEIGHT};

    SDL_RenderCopy(renderer, texture, NULL, &firstImage);
    SDL_RenderCopy(renderer, texture, NULL, &secondImage);
}

void Background::free()
{
    SDL_DestroyTexture(texture);
    texture = NULL;
}
